// init Isotope
var $grid = $('.isotope').isotope({
    "itemSelector": ".isotope-item",
    "layoutMode": "fitRows"
});
// filter items on button click
$('.iso-buttons').on( 'click', 'button', function() {
    var filterValue = $(this).attr('data-filter');
    $grid.isotope({ filter: filterValue });
});